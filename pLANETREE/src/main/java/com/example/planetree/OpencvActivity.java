package com.example.planetree;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Size;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class OpencvActivity extends Activity implements CvCameraViewListener2 {

	private static final String TAG = "OCVSample::Activity";
	Mat hierarchy;

	private CVcameraPreview mOpenCvCameraView;

	// private matchToTemplate mtt;

	// private ProgressDialog pd;
	private final String baseFolder = "Base Images/";
	private final String snapShotsFolder = baseFolder + "Captured Images/";
	private final String resultsFolder = baseFolder + "results/";
	private String[] fileNames = { "Gmelina.jpg", "Narra.jpg", "White lauan.jpg" };
	private int total;

	final Context context = this;
	private ImageView imgView;

	private Mat mRgba;

	Mat touchedRegionRgba;
	int mImageWidth, mImageHeight;
	int[] mRedHistogram = new int[256];
	int[] mGreenHistogram = new int[256];
	int[] mBlueHistogram = new int[256];
	double[] mBinSquared;
	Bitmap mBitmap = null;
	byte[] mYUVData = null;
	int[] mRGBData = null;

	public final int RED = 0;
	public final int GREEN = 1;
	public final int BLUE = 2;

	boolean isColored;
	float offset = 1;
	Paint paint = new Paint();
	Canvas canvas = new Canvas();
	String hex;
	private Drawable position;
	private String name_samplePic;




	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			progress = ProgressDialog
					.show(context, "Loading", "Please Wait...");
			new Thread() {
				public void run() {
					try {
						sleep(1000);
					} catch (Exception e) {
						Log.e("tag", e.getMessage());
					}
					progress.dismiss();
				}
			}.start();
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.i(TAG, "OpenCV loaded successfully");
				mOpenCvCameraView.enableView();
				// mOpenCvCameraView.setOnTouchListener(OpencvActivity.this);
			}
			break;
			default: {
				super.onManagerConnected(status);
			}
			break;
			}
		}
	};

	public OpencvActivity() {
		Log.i(TAG, "Instantiated new " + this.getClass());
	}

	/** Called when the activity is first created. */
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "called onCreate");
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.surface_view);

		mOpenCvCameraView = (CVcameraPreview) findViewById(R.id.tutorial3_activity_java_surface_view);

		mOpenCvCameraView.setCvCameraViewListener(this);

		ImageView camera = (ImageView) findViewById(R.id.camera);
		/* Layout for the camera */
		RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) camera
				.getLayoutParams();
		lp.setMargins(0, 0, -5, 0);
		camera.setLayoutParams(lp);
		downLoadAssetFileToSDCard(fileNames);
		
		Drawable drawable = getResources().getDrawable(R.drawable.no_match);
		noMatchBitmap = ((BitmapDrawable)drawable).getBitmap();
		//noMatchBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.);
		

		camera.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				CaptureImage();
			}
		});
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this,
				mLoaderCallback);
	}

	public void onDestroy() {
		super.onDestroy();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	public void onCameraViewStarted(int width, int height) {
		/*
		 * last = new Mat(height, width, CvType.CV_8UC4); drawing = new
		 * Mat(height, width, CvType.CV_8UC4); mIntermediateMat = new
		 * Mat(height, width, CvType.CV_8UC4); mGray = new Mat(height, width,
		 * CvType.CV_8UC1); hierarchy = new Mat(); mRgba = new Mat(height,
		 * width, CvType.CV_8UC4); mDetector = new ColorBlobDetector();
		 * mSpectrum = new Mat(); mBlobColorRgba = new Scalar(255);
		 * mBlobColorHsv = new Scalar(255); CONTOUR_COLOR = new
		 * Scalar(255,0,0,255);
		 */

	}

	public void onCameraViewStopped() {
		/*
		 * last.release(); mGray.release(); mIntermediateMat.release();
		 * hierarchy.release();
		 */
	}

	/* For the touch event */

	/*
	 * public boolean onTouch(View v, MotionEvent event) { int cols =
	 * mRgba.cols(); int rows = mRgba.rows();
	 * 
	 * int xOffset = (mOpenCvCameraView.getWidth() - cols) / 2; int yOffset =
	 * (mOpenCvCameraView.getHeight() - rows) / 2;
	 * 
	 * int x = (int)event.getX() - xOffset; int y = (int)event.getY() - yOffset;
	 * 
	 * Log.i(TAG, "Touch image coordinates: (" + x + ", " + y + ")");
	 * 
	 * if ((x < 0) || (y < 0) || (x > cols) || (y > rows)) return false;
	 * 
	 * Rect touchedRect = new Rect(); touchedRect.x = (x>4) ? x-4 : 0;
	 * touchedRect.y = (y>4) ? y-4 : 0;
	 * 
	 * touchedRect.width = (x+4 < cols) ? x + 4 - touchedRect.x : cols -
	 * touchedRect.x; touchedRect.height = (y+4 < rows) ? y + 4 - touchedRect.y
	 * : rows - touchedRect.y;
	 * 
	 * Mat touchedRegionRgba = mRgba.submat(touchedRect);
	 * 
	 * Mat touchedRegionHsv = new Mat(); Imgproc.cvtColor(touchedRegionRgba,
	 * touchedRegionHsv, Imgproc.COLOR_RGB2HSV_FULL);
	 * 
	 * // Calculate average color of touched region mBlobColorHsv =
	 * Core.sumElems(touchedRegionHsv); int pointCount =
	 * touchedRect.width*touchedRect.height; for (int i = 0; i <
	 * mBlobColorHsv.val.length; i++) mBlobColorHsv.val[i] /= pointCount;
	 * 
	 * mBlobColorRgba = converScalarHsv2Rgba(mBlobColorHsv);
	 * 
	 * Log.i(TAG, "Touched rgba color: (" + mBlobColorRgba.val[0] + ", " +
	 * mBlobColorRgba.val[1] + ", " + mBlobColorRgba.val[2] + ", " +
	 * mBlobColorRgba.val[3] + ")");
	 * 
	 * mDetector.setHsvColor(mBlobColorHsv);
	 * 
	 * // Imgproc.resize(mDetector.getSpectrum(), mSpectrum, SPECTRUM_SIZE);
	 * 
	 * mIsColorSelected = true;
	 * 
	 * touchedRegionRgba.release(); touchedRegionHsv.release();
	 * 
	 * return false; // don't need subsequent touch events }
	 */
	Mat last;
	Compare refScene;
	SceneDetectData indx;
	ProgressDialog progress;
	boolean resize = false;
	boolean imageOnly = true;
	Bitmap picture = null;
	Bitmap samplePicture = null;
	Bitmap noMatchBitmap = null;
	ArrayList<Compare> scenes = new ArrayList<Compare>();

	Mat compareImg;
	ArrayList<Mat> dataImages = new ArrayList<Mat>();
	ArrayList<MatOfPoint> dataShapes = new ArrayList<MatOfPoint>();

	int thresh = 100;

	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

		mRgba = inputFrame.rgba();

		// return last;
		/*
		 * if (mIsColorSelected) { mDetector.process(mRgba); List<MatOfPoint>
		 * contours = mDetector.getContours(); Log.e(TAG, "Contours count: " +
		 * contours.size()); Imgproc.drawContours(mRgba, contours, -1,
		 * CONTOUR_COLOR);
		 * 
		 * Mat colorLabel = mRgba.submat(4, 68, 4, 68);
		 * colorLabel.setTo(mBlobColorRgba);
		 * 
		 * Mat spectrumLabel = mRgba.submat(4, 4 + mSpectrum.rows(), 70, 70 +
		 * mSpectrum.cols()); mSpectrum.copyTo(spectrumLabel); }
		 */

		return mRgba;

	}

	/*
	 * private Scalar converScalarHsv2Rgba(Scalar hsvColor) { Mat pointMatRgba =
	 * new Mat(); Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
	 * Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL,
	 * 4);
	 * 
	 * return new Scalar(pointMatRgba.get(0, 0)); }
	 */
	public void compareClick(View w) {
		progress = ProgressDialog.show(context, "Scanning Leaf Image",
				"Please Wait!");
		new Thread() {
			public void run() {
				try {
					sleep(1000);
				} catch (Exception e) {
					Log.e("tag", e.getMessage());
				}
				progress.dismiss();
			}
		}.start();
		
		new ComparePics(this).execute();

	}

	boolean showOriginal = true;

	boolean ransacEnabled = true;

	public boolean CaptureImage() {
		Log.i(TAG, "Click event");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		final String currentDateandTime = sdf.format(new Date());
		final String rawFileName = "sample_picture_" + currentDateandTime
				+ ".jpg";

		PictureCallback callback = new PictureCallback() {
			@Override
			public void onPictureTaken(byte[] data, Camera camera) {

				camera.startPreview();
				camera.setPreviewCallback(null);
				Mat m = mRgba.clone();
				compareImg = mRgba.clone();
				dataImages.clear();
				// Mat m = new Mat();
				// Imgproc.cvtColor(im,m, Imgproc.COLOR_BGR2Lab);
				picture = Bitmap.createBitmap(m.cols(), m.rows(),
						Bitmap.Config.ARGB_8888);
				Utils.matToBitmap(m, picture);
				refScene = new Compare(mRgba);

				setContentView(R.layout.preview_image);
				// TextView view = (TextView) findViewById(R.id.textView1);
				// int color = 0xdb8a86,0xd46166;
				/*
				 * Get the pixel value of the image int x = 0; int y= 0 ; int
				 * pixel = picture.getPixel(x, y); int redValue =
				 * Color.red(pixel); int greenValue = Color.green(pixel); int
				 * blueValue = Color.blue(pixel); int[] rgbValues = new int[]{
				 * (pixel >> 24) & 0xff, //alpha (pixel >> 16) & 0xff, //red
				 * (pixel >> 8) & 0xff, //green (pixel ) & 0xff //blue };
				 * 
				 * if(redValue == 192 && greenValue == 0 && blueValue == 0){
				 * view.setText("It is red \\n"+ redValue); }else{
				 * view.setText("Not color red \\n" + rgbValues ); }
				 */

				File file = new File(Environment.getExternalStorageDirectory()
						.getPath()
						+ File.separator
						+ snapShotsFolder
						+ rawFileName);

				if (!file.exists()) {
					try {
						file.createNewFile();

						ByteArrayOutputStream bytes = new ByteArrayOutputStream();
						picture.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

						FileOutputStream fo = new FileOutputStream(file);
						fo.write(bytes.toByteArray());
						fo.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				imgView = (ImageView) findViewById(R.id.ImageView01);
				imgView.setImageBitmap(picture);

			}
		};

		mOpenCvCameraView.takePicture(getFile(snapShotsFolder + rawFileName),
				callback);

		return false;
	}

	private String getFile(String fileName) {
		String filePath = Environment.getExternalStorageDirectory().getPath()
				+ File.separator + fileName;

		return filePath;
	}

	private void downLoadAssetFileToSDCard(String[] fileNames) {

		total = fileNames.length;

		// creates base folder for templates
		File fldr = new File(Environment.getExternalStorageDirectory()
				.getPath() + File.separator + baseFolder);
		// if folder doesn't exist create it
		if (!fldr.exists()) {
			fldr.mkdirs();
		}

		// creates folder in base folder for snapshots taken
		File fldr2 = new File(Environment.getExternalStorageDirectory()
				.getPath() + File.separator + snapShotsFolder);
		// if folder doesn't exist create it
		if (!fldr2.exists()) {
			fldr2.mkdirs();
		}

		// creates folder in base folder for matching results taken
		File fldr3 = new File(Environment.getExternalStorageDirectory()
				.getPath() + File.separator + resultsFolder);
		// if folder doesn't exist create it
		if (!fldr3.exists()) {
			fldr3.mkdirs();
		}

		for (int x = 0; x < total; x++) {
			String fileName = fileNames[x];

			try {
				InputStream bitmap = getAssets().open(fileName);
				Bitmap bit = BitmapFactory.decodeStream(bitmap);

				File file = new File(Environment.getExternalStorageDirectory()
						.getPath() + File.separator + baseFolder + fileName);

				if (!file.exists()) {
					file.createNewFile();

					ByteArrayOutputStream bytes = new ByteArrayOutputStream();
					bit.compress(Bitmap.CompressFormat.PNG, 40, bytes);

					FileOutputStream fo = new FileOutputStream(file);
					fo.write(bytes.toByteArray());
					fo.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	class ComparePics extends AsyncTask<Void, Integer, Bitmap> {
		Context context;

		public ComparePics(Context context) {

			this.context = context;

		}

		protected void onPrexecute() {
			progress = new ProgressDialog(context);
			progress.setCancelable(false);
			progress.setMessage("Identifying the captured image");
			progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progress.setProgress(1);
			progress.setMax(scenes.size());
			progress.show();
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {

			progress.setProgress(values[0]);
			super.onProgressUpdate(values);
		}

		@Override
		protected Bitmap doInBackground(Void... params) {

			long s = System.currentTimeMillis();
			SceneDetectData maxData = null;
			int maxDist = -1;
			int idx = -1;

			for (int x = 0; x < total; x++) {
				String fileName = fileNames[x];

				try {
					InputStream bitmap = getAssets().open(fileName);
					Bitmap bit = BitmapFactory.decodeStream(bitmap);
					Utils.bitmapToMat(bit, mRgba);
					Compare scene = new Compare(mRgba);
					scenes.add(scene);

					detectDataShape(mRgba);

				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			idx = compareShape();

			Mat data = null;
			
			if(idx >= 0){				
				data = dataImages.get(idx);

				Bitmap bmp = Bitmap.createBitmap(data.cols(), data.rows(), Bitmap.Config.ARGB_8888);	
				Imgproc.cvtColor(data, data, Imgproc.COLOR_BGR2RGB);
				Utils.matToBitmap(data, bmp);

				//picture = bmp;
				samplePicture = bmp;
				name_samplePic = "Leaf identified as: \n         " + fileNames[idx].replace(".jpg", "");
			}else{
				samplePicture = noMatchBitmap;
				name_samplePic = "can not identify";
			}
				
			

			return picture;
		}

		public void detectDataShape(Mat image)
		{
			int tempWidth = (int)((double)image.cols() * 0.4);
			int tempHeight = (int)((double)image.rows() * 0.4);

			Mat src = image.clone();
			Mat gray = image.clone();
			Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
			Imgproc.threshold(gray, gray, 200, 255, 1);

			List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
			Mat hierarchy = new Mat();

			Imgproc.findContours( gray, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

			if( contours.size() != 0 )
			{
				Iterator<MatOfPoint> each = contours.iterator();

				while (each.hasNext())
				{
					MatOfPoint wrapper = each.next();
					Rect contourRect = Imgproc.boundingRect( wrapper );

					if ((tempWidth < contourRect.width)&&(tempHeight < contourRect.height))
					{
						dataImages.add(src);
						dataShapes.add(wrapper);
						break;
					}	
				}
			}	
		}

		public int compareShape()
		{
			int ret = -1;
			Size size = new Size(480, 480*compareImg.rows()/compareImg.cols());
			Imgproc.resize(compareImg, compareImg, size);
			List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
			Mat hierarchy = new Mat();
			Mat dilateKernel = new Mat();
			Point dilatePoint = new Point(-1, -1);
			MatOfPoint contour = null;

			int thresh = 30;
			int tempWidth = (int)((double)compareImg.cols() * 0.5);
			int tempHeight = (int)((double)compareImg.rows() * 0.5);

			Mat gray = compareImg.clone();
			Imgproc.cvtColor(compareImg, gray, Imgproc.COLOR_BGR2GRAY);

			Size blurSize = new Size(3, 3);
			Imgproc.blur(gray, gray, blurSize);
			Imgproc.Canny( gray, gray, thresh, thresh*2);
			Imgproc.dilate(gray, gray, dilateKernel, dilatePoint, 2);

			/// Find contours
			Imgproc.findContours(gray, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);	

			if( contours.size() != 0 )
			{
				Iterator<MatOfPoint> each = contours.iterator();

				while (each.hasNext())
				{
					MatOfPoint wrapper = each.next();
					Rect contourRect = Imgproc.boundingRect( wrapper );

					if ((contourRect.width > tempWidth)&&(contourRect.height > tempHeight))
					{
						contour = wrapper;
						break;
					}	
				}
			}

			int dataIdx = -1;
			double val = 0.3;

			if(contour != null)
			{
				double thresholdVal = 1.0;

				Iterator<MatOfPoint> each = dataShapes.iterator();
				int index = 0;
				while (each.hasNext())
				{
					MatOfPoint wrapper = each.next();
					double temp = Imgproc.matchShapes(contour, wrapper, Imgproc.CV_CONTOURS_MATCH_I3, 0.0);
					if(temp < thresholdVal)
					{
						thresholdVal = temp;
						dataIdx = index;
					}
					index++;
				}

				if(thresholdVal < val)
					ret= dataIdx;
			}	

			return ret;
		}

		protected void onPostExecute(Bitmap maxData) {
			// info.setText(result);
			progress.dismiss();
			setContentView(R.layout.detected_layout);

			mBinSquared = new double[256];
			for (int bin = 0; bin < 256; bin++) {
				mBinSquared[bin] = ((double) bin) * bin;
			} // bin

			/*
			 * Layout for the back for imageview RelativeLayout.LayoutParams lp
			 * = (RelativeLayout.LayoutParams)im.getLayoutParams();
			 * lp.setMargins(0, 0, 0, 0); im.setLayoutParams(lp);
			 */
			ImageView im = (ImageView) findViewById(R.id.imagePopup);
			ImageView im1 = (ImageView)findViewById(R.id.imagePopup1);
			TextView info = (TextView)findViewById(R.id.text);
			Button mainmenu = (Button) findViewById(R.id.dismissBtn);

			/* Calculate the mean value of the image */
			int A, R, G, B;
			A = R = G = B = 0;
			int pixelColor;
			int width = picture.getWidth();
			int height = picture.getHeight();
			int size = width * height;

			for (int x1 = 0; x1 < width; ++x1) {
				for (int y1 = 0; y1 < height; ++y1) {
					pixelColor = picture.getPixel(x1, y1);
					A += Color.alpha(pixelColor);
					R += Color.red(pixelColor);
					G += Color.green(pixelColor);
					B += Color.blue(pixelColor);
				}
			}

			A /= size;
			R /= size;
			G /= size;
			B /= size;

			/* Show the hex value */
			// int color = Color.parseColor("#AARRGGBB");
			hex = String.format("#%02x%02x%02x", R, G, B);

			im.setImageBitmap(picture);
			im1.setImageBitmap(samplePicture);
			info.setText(name_samplePic);

			mainmenu.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// settingsDialog.dismiss();
					Intent i = new Intent(OpencvActivity.this,
							AgeActivity.class);
					startActivity(i);
				}
			});

			// if(index.idx == 0){
			// Toast.makeText(context,"It is a carabeef", 5000).show();
			// }else{
			// Toast.makeText(context,"It is a beef", 5000).show();
			// }

			// settingsDialog.show();

			super.onPostExecute(maxData);
		}
	}

	public void onDraw(Canvas canvas) {
		paint.setColor(Color.parseColor(hex));
		paint.setStrokeWidth(3);
		canvas.drawRect(130, 130, 180, 180, paint);
		position.setBounds(10, 20, 30, 40);

		position.draw(canvas);
	}

	public void TemplateMatching() {
		int match_method = Imgproc.TM_CCOEFF;

		String[] fileName = fileNames;

		Mat img = Highgui.imread(getFile(snapShotsFolder));
		Mat templ = Highgui.imread(getFile(Environment
				.getExternalStorageDirectory().getPath()
				+ File.separator
				+ fileName));

		/* Get the size */
		Mat template = new Mat(templ.size(), CvType.CV_32F);
		Imgproc.cvtColor(templ, template, Imgproc.COLOR_BGR2RGBA);

		// / Create the result matrix
		int result_cols = img.cols() - template.cols() + 1;
		int result_rows = img.rows() - template.rows() + 1;
		Mat result = new Mat(result_cols, result_rows, CvType.CV_32FC1);

		// / Do the Matching and Normalize
		// Imgproc.matchTemplate(img, templ, result, match_method);
		// Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new
		// Mat());
		Imgproc.matchTemplate(img, template, result, Imgproc.TM_CCOEFF);
		Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());

		// / Localizing the best match with minMaxLoc
		MinMaxLocResult mmr = Core.minMaxLoc(result);

		Point matchLoc;
		if (match_method == Imgproc.TM_SQDIFF
				|| match_method == Imgproc.TM_SQDIFF_NORMED) {
			matchLoc = mmr.minLoc;
		} else {
			matchLoc = mmr.maxLoc;
		}

		Core.rectangle(img, matchLoc, new Point(matchLoc.x + templ.cols(),
				matchLoc.y + templ.rows()), new Scalar(0, 255, 0));
		System.out.println("Writing " + fileName + "/mediaAppPhotos/result.png");
		Highgui.imwrite(fileName + "/mediaAppPhotos/result.png", img);

		Utils.matToBitmap(img, picture);

	}

}
