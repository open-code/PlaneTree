package com.example.planetree;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.Toast;

public class SceneDetectData {
	int original_key1;
	int original_key2;	
	int original_matches;
	int dist_matches;
	int homo_matches;
	long elapsed;
	int idx;

	Bitmap picture;
	Context context = null;

	@Override
	public String toString() {
		String result = "";

		result +=   "Matched Image Index: " + idx;
		result += "\nTotal Matches: " + original_matches;
		result += "\nDistance Filtered Matches: " + dist_matches;
		result += "\nHomography Filtered Matches: " + homo_matches;
		result += "\nElapsed: (" + elapsed + " ms.)";
		return result;
	}

	public int get(int idx){
		return idx;
	}
}
