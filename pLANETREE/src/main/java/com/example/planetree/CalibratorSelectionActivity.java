package com.example.planetree;

import java.io.IOException;
import java.util.List;

import com.example.planetree.SensorSizeUtil.SizeF;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CalibratorSelectionActivity extends Activity implements TouchView.TouchViewListener {
	public static final float CALLIBRATION_DISTANCE = 12.0f * 25.4f;

	SurfaceView suvCamera;
	SurfaceHolder.Callback sh_callback;
	Camera mCamera;

	ImageButton ibCamera;

	RelativeLayout rlCapture;
	RelativeLayout rlPreview;

	private ImageView ivResult;
	private TouchView tovSelection;
	TextView tvWidth;
	TextView tvHeight;
	TextView tvDistance;
	Button bNext;
	Button bRepick;

	Bitmap resultImage;

	private SizeF objectSize = new SizeF(0, 0); 
	private SizeF sensorSize = new SizeF(0, 0); 

	private int mScreenHeight;
	private int mScreenWidth;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_calibrator_selection);

		Bundle extras = getIntent().getExtras();

		objectSize.width = extras.getFloat("sample-width");
		objectSize.height = extras.getFloat("sample-height");

		getWindow().setFormat(PixelFormat.TRANSLUCENT);

		rlCapture = (RelativeLayout)findViewById(R.id.rlCapturing);
		rlPreview = (RelativeLayout)findViewById(R.id.rlPreview);

		suvCamera = (SurfaceView)findViewById(R.id.suvCamera);
		suvCamera.getHolder().addCallback(my_callback());
		ibCamera = (ImageButton)findViewById(R.id.ibCamera);

		ibCamera.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mCamera.autoFocus(new Camera.AutoFocusCallback() {

					@Override
					public void onAutoFocus(boolean success, Camera camera) {
						if(success){
							camera.takePicture(shutterCallback, rawCallback, jpegCallback);
						}
					}
				});
			}
		});


		ivResult = (ImageView)findViewById(R.id.ivResult);

		tovSelection = (TouchView)findViewById(R.id.tovSelection);
		tovSelection.setListener(this);

		tvWidth = (TextView)findViewById(R.id.tvWidth);
		tvHeight = (TextView)findViewById(R.id.tvHeight);
		tvDistance = (TextView)findViewById(R.id.tvDistance);

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		mScreenHeight = displaymetrics.heightPixels;
		mScreenWidth = displaymetrics.widthPixels;

		Rect rect = new Rect(mScreenWidth / 5, mScreenHeight / 5, mScreenWidth * 4 / 5, mScreenHeight * 4 / 5);
		tovSelection.setRec(rect);

		bNext = (Button)findViewById(R.id.bNext);
		bNext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				Editor editor = pref.edit();
				editor.putFloat("sensor-width", sensorSize.width);
				editor.putFloat("sensor-height", sensorSize.height);
				editor.commit();
				finish();
			}
		});

		bRepick = (Button)findViewById(R.id.bRepick);
		bRepick.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				rlCapture.setVisibility(View.VISIBLE);
				rlPreview.setVisibility(View.GONE);

				mCamera.startPreview();
			}
		});

		AlertDialog.Builder messageBuilder = new AlertDialog.Builder(this);
		messageBuilder.setTitle("Camera Sensor Calibration");
		messageBuilder.setMessage("Fold an 11-inch bond paper to half (cross-wise) and place it in a flat surface.\n" +
				"Rotate the paper to a 90 degree angle with the length longer than the width.\n" +
				"Position the paper where its length-side will be aligned vertically to the screen of the device.\n" +
				"Place the camera 12 inches above the paper and paralleled on to it." +
				"Take a picture of the paper.\n\n" +
				"Adjust the selection to fit the paper. then click next.\n\n" +
				"REMINDER: This process is very important and should be done properly.\n" +
				"Failing to do so will give inaccurate results when calcualting age and dimensions of the tree.");
		messageBuilder.setPositiveButton("OK", null);
		messageBuilder.create().show();
	}

	public void onAfterTouch(TouchView touchView) {
		refreshTexts();
	}

	private void refreshTexts() {
		float rWidth = tovSelection.mRightBottomPosX - tovSelection.mLeftBottomPosX;
		float rHeight = tovSelection.mRightBottomPosY - tovSelection.mRightTopPosY;

		int vWidth = ivResult.getWidth();
		float widthRatio = (float)ivResult.getWidth() / (float)resultImage.getHeight();
		Log.d("width ration", widthRatio + "");
		int vHeight = (int)(resultImage.getHeight() * widthRatio);

		float pWidth = (rWidth / (float)vWidth) * resultImage.getWidth();
		float pHeight = (rHeight / (float)vHeight) * resultImage.getHeight();

		float focalLength = mCamera.getParameters().getFocalLength();

		Log.d("Screen", mScreenWidth + " x " + mScreenHeight);
		Log.d("TouchView", rWidth + " x " + rHeight);
		Log.d("ImageView", vWidth + " x " + vHeight);

		sensorSize.width = (focalLength * objectSize.width * resultImage.getWidth()) / (pWidth * CALLIBRATION_DISTANCE);
		sensorSize.height = (focalLength * objectSize.height * resultImage.getHeight()) / (pHeight * CALLIBRATION_DISTANCE);

		tvWidth.setText("Sensor Width: " + sensorSize.width + "mm");
		tvHeight.setText("Sensor Height: " + sensorSize.height + "mm");
		tvDistance.setText("Sensor Distance: " + String.valueOf(CALLIBRATION_DISTANCE) +"mm");
	}

	Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {

		@Override
		public void onShutter() {
		}
	}; 

	Camera.PictureCallback rawCallback = new Camera.PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
		}
	};

	Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			Options options = new Options();
			options.inDither = false;
			options.inPreferredConfig = Bitmap.Config.RGB_565;

			resultImage = BitmapFactory.decodeByteArray(data, 0, data.length, options);

			rlCapture.setVisibility(View.GONE);
			rlPreview.setVisibility(View.VISIBLE);

			ivResult.setImageBitmap(resultImage);
			refreshTexts();
		}
	};

	SurfaceHolder.Callback my_callback() {
		SurfaceHolder.Callback ob1 = new SurfaceHolder.Callback() {

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				mCamera.stopPreview();
				mCamera.release();
				mCamera = null;
				holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

				mCamera = Camera.open();

				try {// Set camera properties first
					mCamera.setPreviewDisplay(holder);
					Camera.Parameters parameters = mCamera.getParameters();
					parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
					parameters.set("orientation", "portrait");
					parameters.setRotation(90);

					mCamera.setDisplayOrientation(90);

					List<Size> sizes = parameters.getSupportedPictureSizes();
					Size biggest = sizes.get(0);

					for (Size size : sizes) {
						if(size.height > biggest.height){
							biggest = size;
						}
					}

					parameters.setPictureSize(biggest.width, biggest.height);

					DisplayMetrics displaymetrics = new DisplayMetrics();
					getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

					mCamera.setParameters(parameters);

					float ratio = (float)biggest.height / (float)displaymetrics.widthPixels;

					RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)suvCamera.getLayoutParams();
					layoutParams.height = (int)(biggest.width / ratio);
					suvCamera.setLayoutParams(layoutParams);
				} catch (IOException exception) {
					mCamera.release();
					mCamera = null;
				}
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {
				try
				{
					mCamera.startPreview();
				}
				catch(Exception e)

				{
					//e.printStackTrace(err);
				}
			}
		};
		return ob1;
	}

	private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
		final double ASPECT_TOLERANCE = 0.1;
		double targetRatio=(double)h / w;

		if (sizes == null) return null;

		Camera.Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		int targetHeight = h;

		for (Camera.Size size : sizes) {
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}

		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Camera.Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}
		return optimalSize;
	}

}
