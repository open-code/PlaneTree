package com.example.planetree;

import java.io.IOException;

import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;

public class Compare{
	/*For Color Histogram*/
	/*For touching the region of same colors

	    byte[] mYUVData;
		int[] mRGBData;
		int mImageWidth, mImageHeight;
		int[] mRedHistogram;
		int[] mGreenHistogram;
		int[] mBlueHistogram;
		double[] mBinSquared;
		Bitmap picture;

		public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.result);
		ImageView imgView = (ImageView) findViewById(R.id.ImageView02);
		imgView.setImageBitmap(picture);

	}
	//Calculate histogram
	  protected void onDraw(Canvas canvas) {
		  mRGBData = null;
	      mRedHistogram = new int[256];
	      mGreenHistogram = new int[256];
	      mBlueHistogram = new int[256];
	      mBinSquared = new double[256];

	      if (picture != null)
	      {
	    	int canvasWidth = canvas.getWidth();
	      	int canvasHeight = canvas.getHeight();
	      	int newImageWidth = canvasWidth;
	      	int newImageHeight = canvasHeight;
	      	int marginWidth = (canvasWidth - newImageWidth)/2;

		calculateIntensityHistogram(mRGBData, mRedHistogram, 
				mImageWidth, mImageHeight, 0);
		calculateIntensityHistogram(mRGBData, mGreenHistogram, 
				mImageWidth, mImageHeight, 1);
		calculateIntensityHistogram(mRGBData, mBlueHistogram, 
				mImageWidth, mImageHeight, 2);

		// Calculate mean
		double imageRedMean = 0, imageGreenMean = 0, imageBlueMean = 0;
		double redHistogramSum = 0, greenHistogramSum = 0, blueHistogramSum = 0;

		for (int bin = 0; bin < 256; bin++)
		{
			imageRedMean += mRedHistogram[bin] * bin;
			redHistogramSum += mRedHistogram[bin];
			imageGreenMean += mGreenHistogram[bin] * bin;
			greenHistogramSum += mGreenHistogram[bin];
			imageBlueMean += mBlueHistogram[bin] * bin;
			blueHistogramSum += mBlueHistogram[bin];
		} // bin
		imageRedMean /= redHistogramSum;
		imageGreenMean /= greenHistogramSum;
		imageBlueMean /= blueHistogramSum;

		// Calculate second moment
		double imageRed2ndMoment = 0, imageGreen2ndMoment = 0, imageBlue2ndMoment = 0;
		for (int bin = 0; bin < 256; bin++)
		{
			imageRed2ndMoment += mRedHistogram[bin] * mBinSquared[bin];
			imageGreen2ndMoment += mGreenHistogram[bin] * mBinSquared[bin];
			imageBlue2ndMoment += mBlueHistogram[bin] * mBinSquared[bin];
		} // bin
		imageRed2ndMoment /= redHistogramSum;
		imageGreen2ndMoment /= greenHistogramSum;
		imageBlue2ndMoment /= blueHistogramSum;
		double imageRedStdDev = Math.sqrt( imageRed2ndMoment - imageRedMean*imageRedMean );
		double imageGreenStdDev = Math.sqrt( imageGreen2ndMoment - imageGreenMean*imageGreenMean );
		double imageBlueStdDev = Math.sqrt( imageBlue2ndMoment - imageBlueMean*imageBlueMean );

		// Draw mean
		String imageMeanStr = "Mean (R,G,B): " + String.format("%.4g", imageRedMean) + ", " + String.format("%.4g", imageGreenMean) + ", " + String.format("%.4g", imageBlueMean);

		// Draw standard deviation
		String imageStdDevStr = "Std Dev (R,G,B): " + String.format("%.4g", imageRedStdDev) + ", " + String.format("%.4g", imageGreenStdDev) + ", " + String.format("%.4g", imageBlueStdDev);

		// Draw red intensity histogram
		float barMaxHeight = 3000;
		float barWidth = ((float)newImageWidth) / 256;
		float barMarginHeight = 2;
		RectF barRect = new RectF();
		barRect.bottom = canvasHeight - 200;
		barRect.left = marginWidth;
		barRect.right = barRect.left + barWidth;
		for (int bin = 0; bin < 256; bin++)
		{
			float prob = (float)mRedHistogram[bin] / (float)redHistogramSum;
			barRect.top = barRect.bottom - 
				Math.min(80,prob*barMaxHeight) - barMarginHeight;
			//canvas.drawRect(barRect, mPaintBlack);
			barRect.top += barMarginHeight;
			//canvas.drawRect(barRect, mPaintRed);
			barRect.left += barWidth;
			barRect.right += barWidth;
		} // bin

		// Draw green intensity histogram
		barRect.bottom = canvasHeight - 100;
		barRect.left = marginWidth;
		barRect.right = barRect.left + barWidth;
		for (int bin = 0; bin < 256; bin++)
		{
			barRect.top = barRect.bottom - Math.min(80, ((float)mGreenHistogram[bin])/((float)greenHistogramSum) * barMaxHeight) - barMarginHeight;
			//canvas.drawRect(barRect, mPaintBlack);
			barRect.top += barMarginHeight;
			//canvas.drawRect(barRect, mPaintGreen);
			barRect.left += barWidth;
			barRect.right += barWidth;
		} // bin

		// Draw blue intensity histogram
		barRect.bottom = canvasHeight;
		barRect.left = marginWidth;
		barRect.right = barRect.left + barWidth;
		for (int bin = 0; bin < 256; bin++)
		{
			barRect.top = barRect.bottom - Math.min(80, ((float)mBlueHistogram[bin])/((float)blueHistogramSum) * barMaxHeight) - barMarginHeight;
			//canvas.drawRect(barRect, mPaintBlack);
			barRect.top += barMarginHeight;
			//canvas.drawRect(barRect, mPaintBlue);
			barRect.left += barWidth;
			barRect.right += barWidth;
		} // bin
	} // end if statement


	} 
	  static public void calculateIntensityHistogram(int[] rgb, int[] histogram, int width, int height, int component)
	  {
	  	for (int bin = 0; bin < 256; bin++)
	  	{
	  		histogram[bin] = 0;
	  	} // bin
	  	if (component == 0) // red
	  	{
	  		for (int pix = 0; pix < width*height; pix += 3)
	  		{
	      		int pixVal = (rgb[pix] >> 16) & 0xff;
	      		histogram[ pixVal ]++;
	  		} // pix
	  	}
	  	else if (component == 1) // green
	  	{
	  		for (int pix = 0; pix < width*height; pix += 3)
	  		{
	      		int pixVal = (rgb[pix] >> 8) & 0xff;
	      		histogram[ pixVal ]++;
	  		} // pix
	  	}
	  	else // blue
	  	{
	  		for (int pix = 0; pix < width*height; pix += 3)
	  		{
	      		int pixVal = rgb[pix] & 0xff;
	      		histogram[ pixVal ]++;
	  		} // pix
	  	}
	  } **/
	final Mat image;
	final Mat descriptors = new Mat();
	final MatOfKeyPoint keypoints = new MatOfKeyPoint();
	boolean firstTime = true;

	public Compare(Mat image) {
		this.image = image.clone();
		// DetectUtility.analyze(image, keypoints, descriptors);
	}

	public void preCompute() {
		if (firstTime) {
			DetectUtility.analyze(image, keypoints, descriptors);
			firstTime = false;
		}
	}

	public SceneDetectData compare(Compare frame, boolean isHomogrpahy, boolean imageOnly) {
		// Info to store analysis stats
		SceneDetectData s = new SceneDetectData();

		// Detect key points and compute descriptors for inputFrame
		MatOfKeyPoint f_keypoints = frame.keypoints;
		Mat f_descriptors = frame.descriptors;

		this.preCompute();
		frame.preCompute();


		// Compute matches
		MatOfDMatch matches = DetectUtility.match(descriptors, f_descriptors);

		// Filter matches by distance

		MatOfDMatch filtered = DetectUtility.filterMatchesByDistance(matches);

		// If count of matches is OK, apply homography check

		s.original_key1 = (int) descriptors.size().height;
		s.original_key2 = (int) f_descriptors.size().height;

		s.original_matches = (int) matches.size().height;
		s.dist_matches = (int) filtered.size().height;

		if (isHomogrpahy) {
			MatOfDMatch homo = DetectUtility.filterMatchesByHomography(
					keypoints, f_keypoints, filtered);
			Bitmap bmp = DetectUtility.drawMatches(image, keypoints,
					frame.image, f_keypoints, homo, imageOnly);
			s.picture = bmp;
			s.homo_matches = (int) homo.size().height;
			return s;
		} else {
			Bitmap bmp = DetectUtility.drawMatches(image, keypoints,
					frame.image, f_keypoints, filtered, imageOnly);
			s.picture = bmp;
			s.homo_matches = -1;
			return s;

		}

	}
}


