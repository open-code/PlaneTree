package com.example.planetree;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

;
public class Main extends Activity {
	public static final float PAPER_WIDTH = 5.5f * 25.4f;
	public static final float PAPER_HEIGHT = 8.5f * 25.4f; 

	SharedPreferences pref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		ImageButton buttonGo = (ImageButton) findViewById(R.id.imageButton1);
		buttonGo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Main.this, OpencvActivity.class);
				startActivity(i);
			}
		});

		ImageButton but = (ImageButton) findViewById(R.id.imageButton2);
		but.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Main.this, IntroductionActivity.class);
				startActivity(intent);
			}
		});


		float sensorWidth = pref.getFloat("sensor-width", 0.0f);
		float sensorHeight = pref.getFloat("sensor-height", 0.0f);

		if (sensorWidth == 0.0f || sensorHeight == 0.0f) {
			Intent intent = new Intent(this, CalibratorSelectionActivity.class);
			intent.putExtra("sample-width", PAPER_WIDTH);
			intent.putExtra("sample-height", PAPER_HEIGHT);
			startActivity(intent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
