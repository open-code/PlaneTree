package com.example.planetree;

import java.util.HashMap;

import android.os.Build;

public class SensorSizeUtil {
	public static class SizeF{
		public float width;
		public float height;

		public SizeF(){
			this.width = this.height = 0;
		}

		public SizeF(SizeF other){
			this.width = other.width;
			this.height = other.height;
		}

		public SizeF(float width, float height){
			this.width = width;
			this.height = height;
		}

		public void set(float width, float height){
			this.width = width;
			this.height = height;
		}

		public void set(SizeF other){
			this.width = other.width;
			this.height = other.height;
		}

		public float getArea(){
			return width * height;
		}
	}

	private static final HashMap<String, SizeF> rectangles = new HashMap<String, SizeF>();

	static {
		rectangles.put("LGE Nexus 4", new SizeF(3.68f, 2.76f));
		rectangles.put("LGE Nexus 5", new SizeF(4.54f, 3.42f));
		rectangles.put("CherryMobile Burst 2.0", new SizeF(4.54f, 3.42f));
	}

	public static SizeF getSensorSize(String device_model) {
		return rectangles.get(device_model);
	}

	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}

	private static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}
}
