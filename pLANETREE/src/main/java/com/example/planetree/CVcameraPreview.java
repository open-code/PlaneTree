package com.example.planetree;

import java.io.FileOutputStream;

import org.opencv.android.JavaCameraView;

import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;

public class CVcameraPreview extends JavaCameraView implements PictureCallback {

	private static final String TAG = "Sample::Tutorial3View";
	private String mPictureFileName;
	private byte[] captureData;
	private boolean dataReady = false;

	public CVcameraPreview(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Camera getCamera() {
		return mCamera;
	}

	public byte[] getData() {
		return captureData;
	}

	public boolean isDataReady() {
		return dataReady;
	}

	public void takePicture(final String fileName) {
		Log.i(TAG, "Taking picture");
		this.mPictureFileName = fileName;

		mCamera.setPreviewCallback(null);

		mCamera.takePicture(null, null, this);
	}

	public void takePicture(final String fileName, PictureCallback callback) {
		Log.i(TAG, "Taking picture");
		this.mPictureFileName = fileName;

		mCamera.setPreviewCallback(null);

		mCamera.takePicture(null, null, callback);
	}    

	@Override
	public void onPictureTaken(byte[] data, Camera camera) {
		Log.i(TAG, "Saving a bitmap to file");
		// The camera preview was automatically stopped. Start it again.
		mCamera.startPreview();
		mCamera.setPreviewCallback(this);

		// Write the image in a file (in jpeg format)
		try {
			FileOutputStream fos = new FileOutputStream(mPictureFileName);

			fos.write(data);
			fos.close();

		} catch (java.io.IOException e) {
			Log.e("PictureDemo", "Exception in photoCallback", e);
		}
	}
}
