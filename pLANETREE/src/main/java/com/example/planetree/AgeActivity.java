package com.example.planetree;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.planetree.SensorSizeUtil.SizeF;

public class AgeActivity extends Activity implements SensorEventListener{

	float pitch;  // View to draw a compass
	SharedPreferences pref;

	SurfaceView suvCamera;
	SurfaceHolder.Callback sh_callback;
	Camera mCamera;

	// ImageButton ibCamera;

	RelativeLayout rlCapture;
	RelativeLayout rlPreview;

	private ImageView ivResult;
	private TouchView tovSelection;
	private TextView tvDistance;
	private TextView tvCaption;
	private TextView typeTree;
	private TextView hTree;
	private TextView wTree;
	private TextView dTree;
	private TextView aTree;
	private ImageView ivCrossHair;
	private Button bNext;
	private Button bAccept;
	private Button bBack;
	private Button btnCalcAge;
	ImageButton camButton;
	private LinearLayout resultLayout;

	Bitmap resultImage;
	SizeF imageSize = new SizeF();
	SizeF biggestImageSize;

	private SizeF objectSize = new SizeF(0, 0);
	private SizeF sensorSize = new SizeF(0, 0);

	private int mScreenHeight;
	private int mScreenWidth;

	private float mHcamera;
	private double mDistance;
	private String mStrDistance="";
	private String mStrTreeType="";

	private float growthFactor = 8.7f;

	private SensorManager mSensorManager;
	Sensor accelerometer;
	Sensor magnetometer;
	private boolean mChecked_Distance = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_age);

		mChecked_Distance = false;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mHcamera = extras.getFloat("deviceH");
			mStrTreeType = extras.getString("treeType");
		}

		// the accelerometer is used for autofocus
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

		getWindow().setFormat(PixelFormat.TRANSLUCENT);

		pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		rlCapture = (RelativeLayout) findViewById(R.id.rlCapturing);
		rlPreview = (RelativeLayout) findViewById(R.id.rlPreview);


		resultLayout = (LinearLayout)findViewById(R.id.resultwindow);
		resultLayout.setVisibility(View.GONE);

		suvCamera = (SurfaceView) findViewById(R.id.suvCamera);
		suvCamera.getHolder().addCallback(my_callback());
		suvCamera.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		typeTree = (TextView)findViewById(R.id.tree_type);
		wTree = (TextView)findViewById(R.id.tree_width);
		hTree = (TextView)findViewById(R.id.tree_height);
		dTree = (TextView)findViewById(R.id.tree_distance);
		aTree = (TextView)findViewById(R.id.tree_age);
		
		ivCrossHair = (ImageView)findViewById(R.id.ivCrossHair);

		camButton = (ImageButton)findViewById(R.id.btnCamera);
		camButton.setVisibility(View.VISIBLE);

		camButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mCamera.autoFocus(new Camera.AutoFocusCallback() {

					@Override
					public void onAutoFocus(boolean success, Camera camera) {
						if(success){
							camera.takePicture(shutterCallback, rawCallback, jpegCallback);
						}
					}
				});


			}
		});

		tvCaption = (TextView)findViewById(R.id.tvCaption);

		bAccept = (Button)findViewById(R.id.btnAccept);
		bAccept.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resultLayout.setVisibility(View.GONE);				
			}
		});

		bBack = (Button)findViewById(R.id.btnBack);
		bBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();				
			}
		});

		ivResult = (ImageView) findViewById(R.id.ivResult);
		tovSelection = (TouchView) findViewById(R.id.tovSelection);
		tovSelection.setVisibility(View.GONE);
		tovSelection.setEnabled(false);
		tvDistance = (TextView)findViewById(R.id.tvDistance);

		btnCalcAge = (Button)findViewById(R.id.btnCalcAge);
		btnCalcAge.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				rlCapture.setVisibility(View.VISIBLE);
				rlPreview.setVisibility(View.GONE);
				mCamera.startPreview();

				camButton.setVisibility(View.VISIBLE);
				camButton.setEnabled(true);
				btnCalcAge.setVisibility(View.GONE);
				btnCalcAge.setEnabled(false);
				
				tovSelection.setVisibility(View.VISIBLE);
				tovSelection.setEnabled(true);
			}
		});
		btnCalcAge.setVisibility(View.GONE);

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		mScreenHeight = displaymetrics.heightPixels;
		mScreenWidth = displaymetrics.widthPixels;

		Rect rect = new Rect(mScreenWidth / 5, mScreenHeight / 5, mScreenWidth * 3 / 5, mScreenHeight * 3 / 5);
		tovSelection.setRec(rect);

		mHcamera = pref.getFloat("distance", 1);

		sensorSize.width = pref.getFloat("sensor-width", 0);
		sensorSize.height = pref.getFloat("sensor-height", 0);
	}

	private void setValues(){
		//calcuate distance between camera and tree
		float  threshold= 0;
		if(pitch <threshold){
			mDistance = Math.tan(Math.abs(pitch)) * (mHcamera)*1000;
			mStrDistance = precision_double(2, mDistance/1000);
			tvDistance.setText(mStrDistance + "meter(s)");
			camButton.setEnabled(true);
		}
		else{
			tvDistance.setText("Invalid Inclination");
			camButton.setEnabled(false);
		}
			
	}

	private double getDegreeOf(Float radian){
		double ret = 0;
		ret = Math.toDegrees(radian.doubleValue());		
		return ret;
	}

	public void onAfterTouch(TouchView touchView) {
		refreshTexts();
	}

	private void refreshTexts() {

		if (imageSize.getArea() != 0) {
			float rWidth = tovSelection.mRightBottomPosX - tovSelection.mLeftBottomPosX;
			float rHeight = tovSelection.mRightBottomPosY - tovSelection.mRightTopPosY;

			int iHeight = (int)imageSize.height;
			int iWidth = (int)imageSize.width;

			int vWidth = suvCamera.getWidth();
			int vHeight = suvCamera.getHeight();

			float pWidth = (rWidth / (float) vWidth) * iWidth;
			float pHeight = (rHeight / (float) vHeight)	* iHeight;

			float focalLength = mCamera.getParameters().getFocalLength();

			objectSize.width = (pWidth * sensorSize.width * (float)mDistance)	/ (focalLength * iWidth);
			objectSize.height = (pHeight * sensorSize.height * (float)mDistance)/ (focalLength * iHeight);

			float widthInches = objectSize.width / 25.4f;

			typeTree.setText(mStrTreeType);
			wTree.setText(String.valueOf(precision_float(2, widthInches)) + " inch(es)");
			hTree.setText(String.valueOf(precision_float(2,objectSize.height / 1000))+ " meter(s)");
			aTree.setText(String.valueOf(precision_double(2, widthInches * (growthFactor / 2.54))) + " year(s)");
			dTree.setText(mStrDistance + "meter(s)");
		}
	}

	Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {

		@Override
		public void onShutter() {
		}
	};

	Camera.PictureCallback rawCallback = new Camera.PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			imageSize.set(biggestImageSize);
		}
	};

	Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			Options options = new Options();
			options.inDither = false;
			options.inPreferredConfig = Bitmap.Config.RGB_565;

			resultImage = BitmapFactory.decodeByteArray(data, 0, data.length, options);

			rlCapture.setVisibility(View.GONE);
			rlPreview.setVisibility(View.VISIBLE);

			ivResult.setImageBitmap(resultImage);

			setVisible();
			setValues();
		}
	};

	private void setVisible(){
		if(!mChecked_Distance){
			//tvDistance.setVisibility(View.VISIBLE);			
			mChecked_Distance = true;
			btnCalcAge.setVisibility(View.VISIBLE);
			ivCrossHair.setVisibility(View.GONE);

		}else{
			refreshTexts();
			rlCapture.setVisibility(View.GONE);
			resultLayout.setVisibility(View.VISIBLE);
			tovSelection.setVisibility(View.GONE);
			tovSelection.setEnabled(false);
		}
		unregisterSensorManager();
		camButton.setVisibility(View.GONE);
		camButton.setEnabled(false);
	}

	public void unregisterSensorManager(){
		mSensorManager.unregisterListener(this);
	}

	SurfaceHolder.Callback my_callback() {
		SurfaceHolder.Callback ob1 = new SurfaceHolder.Callback() {

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				mCamera.stopPreview();
				mCamera.release();
				mCamera = null;
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

				mCamera = Camera.open();

				try {// Set camera properties first
					mCamera.getParameters().getSupportedPreviewSizes();
					//Log.d(par1.toString(), "default parameter of camera");
					mCamera.setPreviewDisplay(holder);
					Camera.Parameters parameters = mCamera.getParameters();
					parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
					parameters.set("orientation", "portrait");
					parameters.setRotation(90);

					List<Size> sizes = parameters.getSupportedPictureSizes();
					Size biggest = sizes.get(0);

					for (Size size : sizes) {
						if (size.height > biggest.height) {
							biggest = size;
						}
					}

					parameters.setPictureSize(biggest.width, biggest.height);

					biggestImageSize = new SizeF(biggest.height, biggest.width);

					DisplayMetrics displaymetrics = new DisplayMetrics();
					getWindowManager().getDefaultDisplay().getMetrics(
							displaymetrics);

					mCamera.setParameters(parameters);

					mCamera.setDisplayOrientation(90);

					float ratio = (float) biggest.height
							/ (float) displaymetrics.widthPixels;

					RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) suvCamera
							.getLayoutParams();
					layoutParams.height = (int) (biggest.width / ratio);
					suvCamera.setLayoutParams(layoutParams);

					imageSize.set(displaymetrics.widthPixels, (biggest.width / ratio));				

					refreshTexts();
				} catch (IOException exception) {
					mCamera.release();
					mCamera = null;
				}
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {
				holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
				float angle = mCamera.getParameters().getVerticalViewAngle();
				mCamera.startPreview();

				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				Log.d("hello", "hello");
			}
		};
		////////////////////////////////////////////////////////////////////
		return ob1;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_FASTEST);
	}

	protected void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(this);
	}

	private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
		final double ASPECT_TOLERANCE = 0.1;
		double targetRatio = (double) h / w;

		if (sizes == null)
			return null;

		Camera.Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		int targetHeight = h;

		for (Camera.Size size : sizes) {
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
				continue;
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}

		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Camera.Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}
		return optimalSize;
	}

	float[] mGravity;
	float[] mGeomagnetic;
	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
			mGravity = event.values;
		if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
			mGeomagnetic = event.values;
		if (mGravity != null && mGeomagnetic != null) {
			float R[] = new float[9];
			float I[] = new float[9];
			boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
			if (success) {
				float orientation[] = new float[3];
				SensorManager.getOrientation(R, orientation);
				pitch = orientation[1]; // orientation contains: azimut, pitch and roll

				setValues();				
			}
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}
	
	public static String precision_float(int decimalPlace, Float d) {

		BigDecimal bd = new BigDecimal(Float.toString(d));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.toString();
	}
	
	public static String precision_double(int decimalPlace, Double d) {

		BigDecimal bd = new BigDecimal(Double.toString(d));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.toString();
	}
}
