package com.example.planetree;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;


public class IntroductionActivity extends Activity {

	SharedPreferences pref;
	private float growthFactor = 8.7f;
	final String myList[] = new String[]{ "white lauan", "gmelina", "narra"};
	
//	private LinearLayout resultLayout;
	private LinearLayout introductionLayout;
	private Button bStart;
	private Button bAccept;
	private Button bBack;
	private String mStrTreeType="";
	private float mHcamera=1.5f;
	private Intent intent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_introduction);
		
		intent = new Intent(IntroductionActivity.this, AgeActivity.class);
		pref = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		
//		resultLayout = (LinearLayout)findViewById(R.id.resultwindow);
//		resultLayout.setVisibility(View.GONE);

		introductionLayout = (LinearLayout)findViewById(R.id.introduction);
		//introductionLayout.setVisibility(View.GONE);
		
		final AlertDialog.Builder ad = new AlertDialog.Builder(this);

		ad.setIconAttribute(android.R.attr.alertDialogIcon);
		ad.setTitle("What's the type of tree?");
		ad.setSingleChoiceItems(myList, -1,  new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {//arg0:dialog interface, arg1: selected point

				mStrTreeType = myList[arg1];
				growthFactor = 13.8f;
				switch(arg1)
				{
				case 0:
					growthFactor = 8.7f;
					break;
				case 1:
					growthFactor = 13.8f;
					break;
				case 2:
					growthFactor = 8.9f;
					break;
				case 3:
					growthFactor = 5.0f * 2.54f;
					break;					   
				}

				Editor editor = pref.edit();
				editor.putInt("selected-tree", arg1);
				editor.commit();

//				refreshTexts();
				arg0.cancel();


				//introductionLayout.setVisibility(View.VISIBLE);
			}
		});

		ad.show();
		
		
		bStart = (Button)findViewById(R.id.btnStart);
		bStart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//introductionLayout.setVisibility(View.GONE);

				//input window for displaying camera height from the grount  
				showDevHeightInputWnd();
			}
		});
		
	}
	
	public void showDevHeightInputWnd() {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				IntroductionActivity.this);
		builder.setTitle("Set Camera Height(m)");

		final EditText input = new EditText(IntroductionActivity.this);
		input.setInputType(InputType.TYPE_CLASS_NUMBER	| InputType.TYPE_NUMBER_FLAG_DECIMAL);
		input.setText((mHcamera) + "");
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton("OK",	new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				try {
					String text = input.getText().toString();
					mHcamera = Float.valueOf(text);

					Editor editor = pref.edit();
					editor.putFloat("distance", mHcamera);
					editor.commit();					
					
					intent.putExtra("deviceH",mHcamera);
					intent.putExtra("treeType", mStrTreeType);
					startActivity(intent);
										
//					refreshTexts();
				} catch (Exception ex) {
				}

			}
		});
		builder.setNegativeButton("Cancel",	new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				startActivity(intent);
				dialog.cancel();
			}
		});

		builder.show();
	}

}
